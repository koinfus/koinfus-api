# Brisage

## POST 

### Récupérer les meilleurs caractéristiques à Focus:

``` json
```

### Différentes valeurs possible de caractéristiques

``` json
"PA"
"PM"
"PO"
"Invocation"
"Dommage"
"Dommmages d'armes"
"Dommages distance"
"Dommages mêlée"
"Dommages aux sorts"
"Résistance distance"
"Résistance mêlée"
"Soin"
"Critique"
"Renvoi de dommage"
"Retrait PM"
"Retrait PA"
"Esquive PA"
"Esquive PM"
"% Resistance Feu"
"% Resistance Eau"
"% Resistance Terre"
"% Resistance Air"
"% Resistance Neutre"
"Dommage Terre"
"Dommage Air"
"Dommage Eau"
"Dommage Feu",
"Dommage Neutre"
"Dommage Critique"
"Dommage de poussé"
"Dommage piège"
"Chasse"
"Tacle"
"Fuite"
"Prospection"
"Sagesse
"Puissance Piège"
"Puissance"
"Résistance Terre"
"Résistance Eau"
"Résistance Air"
"Résistance Neutre"
"Résistance Feu"
"Résistance Critique"
"Résistance Poussé"
"Intelligence"
"Chance"
"Agilité"
"Pods"
"Vitalité"
"Initiative"
```
