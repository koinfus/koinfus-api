﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Koinfus.Application.Common.Interfaces.Services;
using Koinfus.Application.Focus.Queries.Common;

namespace Koinfus.Infrastructure.Services
{
    public class FocusCalculator : IFocusCalculator
    {
        public List<FocusValues> GetFocusValues(List<Characteristic> characteristics)
        {
            var coef = 313;
            var objLvl = 96;
            var res = new List<FocusValues>();

            foreach (var characteristic in characteristics)
            {
                var factor = GetMultiplyingFactorCharacteristic(characteristic.Name);
                var weight = 3 * characteristic.Value * objLvl * factor / 200 +1;
                res.Add(new FocusValues(characteristic.Name, characteristic.Value, 0, weight, 0));
            }

            for (var i = 0; i < res.Count - 1; i++)
            {
                double otherWeight = 0;
                for (var j = 0; j < res.Count; j++)
                {
                    if (i != j)
                    {
                        otherWeight += res[j].PotentialRunesPerHundredPercent;
                    }
                }

                var firstStep = res[i].PotentialRunesPerHundredPercent + 0.5 * otherWeight;

                res[i].PotentialRunesPerHundredPercentWithFocus = Convert.ToInt32(firstStep / GetMultiplyingFactorCharacteristic(res[i].CharacName) * coef / 100);
            }

            return res;
        }

        private double GetMultiplyingFactorCharacteristic(string characName)
        {
            return characName switch
            {
                "PA" => 100,
                "PM" => 90,
                "PO" => 51,
                "Invocation" => 30,
                "Dommages" => 20,
                "Dommmages d'armes" => 15,
                "Dommages distance" => 15,
                "Dommages mêlée" => 15,
                "Dommages aux sorts" => 15,
                "Résistance distance" => 15,
                "Résistance mêlée" => 15,
                "Soin" => 10,
                "Critique" => 10,
                "Renvoi de dommage" => 10,
                "Retrait PM" => 7,
                "Retrait PA" => 7,
                "Esquive PA" => 7,
                "Esquive PM" => 7,
                "% Resistance Feu" => 6,
                "% Resistance Eau" => 6,
                "% Resistance Terre" => 6,
                "% Resistance Air" => 6,
                "% Resistance Neutre" => 6,
                "Dommage Terre" => 5,
                "Dommage Air" => 5,
                "Dommage Eau" => 5,
                "Dommage Feu" => 5,
                "Dommage Neutre" => 5,
                "Dommage Critique" => 5,
                "Dommage de poussé" => 5,
                "Dommage piège" => 5,
                "Chasse" => 5,
                "Tacle" => 4,
                "Fuite" => 4,
                "Prospection" => 3,
                "Sagesse" => 3,
                "Puissance Piège" => 2,
                "Puissance" => 2,
                "Résistance Terre" => 2,
                "Résistance Eau" => 2,
                "Résistance Air" => 2,
                "Résistance Neutre" => 2,
                "Résistance Feu" => 2,
                "Résistance Critique" => 2,
                "Résistance Poussé" => 2,
                "Intelligence" => 1,
                "Chance" => 1,
                "Agilité" => 1,
                "Force" => 1,
                "Pods" => 0.25,
                "Vitalité" => 0.2,
                "Initiative" => 0.1
            };
        }
    }
}
