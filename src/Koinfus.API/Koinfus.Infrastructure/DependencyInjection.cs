﻿using Koinfus.Application.Common.Interfaces.Services;
using Koinfus.Infrastructure.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Koinfus.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(
            this IServiceCollection services,
            ConfigurationManager configuration)
        {
            services
                .AddPersistance();

            services.AddSingleton<IFocusCalculator, FocusCalculator>();

            return services;
        }

        public static IServiceCollection AddPersistance(
            this IServiceCollection services)
        {
            //services.AddDbContext<HotelManagementDbContext>(options =>
            //    options.UseSqlServer("Server=163.172.221.134;Initial Catalog=HotelManagement;User Id=sa;Password=MyPassword123!!;TrustServerCertificate=True;"));

            //services.AddScoped<PublishDomainEventsInterceptor>();
            //services.AddScoped<IUserRepository, UserRepository>();
            //services.AddScoped<IRoomRepository, RoomRepository>();
            //services.AddScoped<IReservationRepository, ReservationRepository>();
            return services;
        }
    }
}
