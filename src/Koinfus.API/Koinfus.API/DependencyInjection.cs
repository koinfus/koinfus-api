﻿using Koinfus.API.Common.Errors;
using Koinfus.API.Common.Mapping;
using Microsoft.AspNetCore.Mvc.Infrastructure;

namespace Koinfus.API
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddPresentation(this IServiceCollection services)
        {
            services.AddControllers();
            services.AddSingleton<ProblemDetailsFactory, KoinfusProblemDetailsFactory>();
            services.AddMappings();

            return services;
        }
    }
}
