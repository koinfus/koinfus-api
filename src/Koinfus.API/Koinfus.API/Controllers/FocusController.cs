﻿using Koinfus.Application.Focus.Queries.Common;
using Koinfus.Application.Focus.Queries.GetFocusInformations;
using Koinfus.Contracts.Focus;
using MapsterMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Koinfus.API.Controllers
{
    [Route("focus")]
    public class FocusController : ApiController
    {
        private readonly ISender _mediator;
        private readonly IMapper _mapper;

        public FocusController(ISender mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpPost("getInfos")]
        public async Task<IActionResult> GetFocusInformations([FromBody]GetFocusInformationsRequest request)
        {
            var query = _mapper.Map<GetFocusInformationsQuery>(request);
            var result = await _mediator.Send(query);
            
            return result.Match(
                focusInfos => Ok(_mapper.Map< GetFocusInformationsResponse>(focusInfos)),
                errors => Problem(errors));
        }
    }
}
