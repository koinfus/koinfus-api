﻿using Koinfus.Application.Focus.Queries.Common;
using Koinfus.Application.Focus.Queries.GetFocusInformations;
using Koinfus.Contracts.Focus;
using Mapster;
using Microsoft.AspNetCore.Identity.Data;

namespace Koinfus.API.Common.Mapping
{
    public class FocusMappingConfig : IRegister
    {
        public void Register(TypeAdapterConfig config)
        {
            config.NewConfig<GetFocusInformationsRequest, GetFocusInformationsQuery>()
                .Map(dest => dest.Characteristics, src => src.Characteristics.Adapt<List<Characteristic>>());

            config.NewConfig<GetFocusResult, GetFocusInformationsResponse>()
                .Map(dest => dest.CharacteristicsBreakingInfos, src => src.FocusValues);
        }
    }
}
