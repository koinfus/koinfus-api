﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koinfus.Contracts.Focus
{
    public record GetFocusInformationsResponse(List<CharacteristicsBreakingInfo> CharacteristicsBreakingInfos);
}
