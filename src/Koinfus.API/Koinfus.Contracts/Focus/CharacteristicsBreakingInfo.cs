﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koinfus.Contracts.Focus
{
    public record CharacteristicsBreakingInfo(
        string CharacName,
        int CharacValue, 
        int CharRanking,
        int PotentialRunesPerHundredPercent,
        int PotentialRunesPerHundredPercentWithFocus);
}
