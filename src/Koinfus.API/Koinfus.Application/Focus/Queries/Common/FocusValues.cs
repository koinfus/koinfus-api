﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koinfus.Application.Focus.Queries.Common
{
    public class FocusValues
    {
        public string CharacName { get; set; }

        public int CharacValue { get; set; }

        public int CharRanking { get; set; }

        public double PotentialRunesPerHundredPercent { get; set; }

        public int PotentialRunesPerHundredPercentWithFocus { get; set; }

        public FocusValues(
            string characName,
            int characValue,
            int charRanking,
            double potentialRunesPerHundredPercent,
            int potentialRunesPerHundredPercentWithFocus)
        {
            CharacName = characName;
            CharacValue = characValue;
            PotentialRunesPerHundredPercent = potentialRunesPerHundredPercent;
        }
    };
}
