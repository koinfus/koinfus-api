﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koinfus.Application.Focus.Queries.Common
{
    public record Characteristic(string Name, int Value);
}
