﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ErrorOr;
using Koinfus.Application.Focus.Queries.Common;
using MediatR;

namespace Koinfus.Application.Focus.Queries.GetFocusInformations
{
    public record GetFocusInformationsQuery(List<Characteristic> Characteristics) : IRequest<ErrorOr<GetFocusResult>>;
}
