﻿using MediatR;
using ErrorOr;
using Koinfus.Application.Common.Interfaces.Services;
using Koinfus.Application.Focus.Queries.Common;

namespace Koinfus.Application.Focus.Queries.GetFocusInformations
{
    public class GetFocusInformationsQueryHandler :
        IRequestHandler<GetFocusInformationsQuery, ErrorOr<GetFocusResult>>
    {
        private readonly IFocusCalculator _focusCalculator;

        public GetFocusInformationsQueryHandler(
            IFocusCalculator focusCalculator)
        {
            _focusCalculator = focusCalculator;
        }

        public async Task<ErrorOr<GetFocusResult>> Handle(GetFocusInformationsQuery request,
            CancellationToken cancellationToken)
        {
            await Task.CompletedTask;

            var result = _focusCalculator.GetFocusValues(request.Characteristics);

            return new GetFocusResult(result);
        }
    }
}
