﻿using Koinfus.Application.Focus.Queries.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Koinfus.Application.Common.Interfaces.Services
{
    public interface IFocusCalculator
    {
        List<FocusValues> GetFocusValues(List<Characteristic> characteristics);
    }
}
